@extends('layouts.default')

@section('title', 'Blog')

@section('header')
    <div class="top-left">
        <img src="{{ asset('images/logo.svg') }}" alt="Next Levels">
    </div>

    @include('partials.navigation')
@endsection

@section('content')
    <div class="page-content">

    </div>
@endsection