<nav>
    <ul class="navigation bottom-right social">
        <li>
            <a href="https://www.facebook.com/NextLevelsNews" target="_blank" rel="noopener">
                <i class="fa fa-facebook-official fa-2x" aria-hidden="true"></i>
                <span>Facebook</span>
            </a>
        </li>
        <li>
            <a href="https://www.instagram.com/nextlevelsnews" target="_blank" rel="noopener">
                <i class="fa fa-instagram fa-2x" aria-hidden="true"></i>
                <span>Instragram</span>
            </a>
        </li>
        <li>
            <a href="https://www.youtube.com/user/NextLevelsChannel" target="_blank" rel="noopener">
                <i class="fa fa-youtube-play fa-2x" aria-hidden="true"></i>
                <span>YouTube</span>
            </a>
        </li>
        <li>
            <a href="https://plus.google.com/+Next-levelsDe/posts" target="_blank" rel="noopener">
                <i class="fa fa-google-plus fa-2x" aria-hidden="true"></i>
                <span>Google+</span>
            </a>
        </li>
        <li>
            <a href="https://twitter.com/nextlevelsnews" target="_blank" rel="noopener">
                <i class="fa fa-twitter fa-2x" aria-hidden="true"></i>
                <span>Twitter</span>
            </a>
        </li>
    </ul>
</nav>