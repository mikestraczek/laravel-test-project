@extends('layouts.default')

@section('title', 'Impressum')

@section('header')
    <div class="top-left">
        <img src="{{ asset('images/logo.svg') }}" alt="Next Levels">
    </div>

    @include('partials.navigation')
@endsection

@section('content')
    <div class="page-content">
        <h4>ANGABEN GEMÄSS § 5 TMG</h4>

        <p>
            Next Levels GmbH<br>
            Heinz-Nixdorf-Straße 12<br>
            41179 Mönchengladbach<br>
        </p>

        <p>
            GESCHÄFTSFÜHRUNG<br>

            Vjaceslav Ditzel<br>
            Paul Kalisch
        </p>

        <p>
            KONTAKT<br><br>

            Telefon:	+49 (0) 2161 5397160<br>
            E-Mail:	info@next-levels.de<br>
        </p>

        <p>
            Umsatzsteuer-Identifikationsnummer gemäß §27 a Umsatzsteuergesetz:<br>
            DE311131426
        </p>

        <p>
            HAFTUNGSAUSSCHLUSS<br><br>

            Als Diensteanbieter sind wir gemäß § 7 Abs.1 TMG für eigene Inhalte auf diesen Seiten nach den allgemeinen Gesetzen verantwortlich. Nach §§ 8 bis 10 TMG sind wir als Diensteanbieter jedoch nicht verpflichtet, übermittelte oder gespeicherte fremde Informationen zu überwachen oder nach Umständen zu forschen, die auf eine rechtswidrige Tätigkeit hinweisen. Verpflichtungen zur Entfernung oder Sperrung der Nutzung von Informationen nach den allgemeinen Gesetzen bleiben hiervon unberührt. Eine diesbezügliche Haftung ist jedoch erst ab dem Zeitpunkt der Kenntnis einer konkreten Rechtsverletzung möglich. Bei Bekanntwerden von entsprechenden Rechtsverletzungen werden wir diese Inhalte umgehend entfernen.
            <br><br>
            Haftung für Links<br>
            Unser Angebot enthält Links zu externen Webseiten Dritter, auf deren Inhalte wir keinen Einfluss haben. Deshalb können wir für diese fremden Inhalte auch keine Gewähr übernehmen. Für die Inhalte der verlinkten Seiten ist stets der jeweilige Anbieter oder Betreiber der Seiten verantwortlich. Die verlinkten Seiten wurden zum Zeitpunkt der Verlinkung auf mögliche Rechtsverstöße überprüft. Rechtswidrige Inhalte waren zum Zeitpunkt der Verlinkung nicht erkennbar. Eine permanente inhaltliche Kontrolle der verlinkten Seiten ist jedoch ohne konkrete Anhaltspunkte einer Rechtsverletzung nicht zumutbar. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Links umgehend entfernen.
            <br><br>
            Urheberrecht<br>
            Die durch die Seitenbetreiber erstellten Inhalte und Werke auf diesen Seiten unterliegen dem deutschen Urheberrecht. Die Vervielfältigung, Bearbeitung, Verbreitung und jede Art der Verwertung außerhalb der Grenzen des Urheberrechtes bedürfen der schriftlichen Zustimmung des jeweiligen Autors bzw. Erstellers. Downloads und Kopien dieser Seite sind nur für den privaten, nicht kommerziellen Gebrauch gestattet. Soweit die Inhalte auf dieser Seite nicht vom Betreiber erstellt wurden, werden die Urheberrechte Dritter beachtet. Insbesondere werden Inhalte Dritter als solche gekennzeichnet. Sollten Sie trotzdem auf eine Urheberrechtsverletzung aufmerksam werden, bitten wir um einen entsprechenden Hinweis. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Inhalte umgehend entfernen.
        </p>




        LINKS ZU ANDEREN INHALTEN

        Facebook: https://www.facebook.com/NextLevelsNews
        Twitter: https://twitter.com/nextlevelsnews
        Google +: https://plus.google.com/+Next-levelsDe/posts
        Youtube: https://www.youtube.com/user/NextLevelsChannel
    </div>
@endsection