<div class="side-menu-header">
    <img class="img-responsive img-circle side-menu-header-img" src="{{ asset('images/admin/profile-image.jpg') }}">
    <div class="side-menu-header-text">{{ Auth::user()->name }}</div>
</div>

<ul class="navigation">
    <li>
        <a href="{{ route('admin-home') }}" class="{{ Route::is('admin-home') ? 'active' : '' }}">
            <i class="fa fa-tachometer" aria-hidden="true"></i>
            <span>Dashboard</span>
        </a>
    </li>
    <li>
        <a href="#" class="{{ Route::is('admin-blog') ? 'active' : '' }}">
            <i class="fa fa-file-text pull-left"></i>
            <span>Blog</span>
            <i class="fa fa-angle-left"></i>
        </a>
        <ul class="sub-navigation">
            <li>
                <a href="{{ route('new-post') }}" class="{{ Route::is('new-post') ? 'active' : '' }}">
                    <i class="fa fa-plus"></i>
                    <span>Post erstellen</span>
                </a>
            </li>
            <li>
                <a href="{{ route('posts') }}" class="{{ Route::is('posts') ? 'active' : '' }}">
                    <i class="fa fa-folder"></i>
                    <span>Alle Posts</span>
                </a>
            </li>
        </ul>
    </li>
    <li>
        <a href="{{ route('admin-reference') }}" class="{{ Route::is('admin-reference') ? 'active' : '' }}">
            <i class="fa fa-binoculars"></i>
            <span>Referenzen</span>
        </a>
    </li>
    <li>
        <a href="{{ route('admin-performance') }}" class="{{ Route::is('admin-performance') ? 'active' : '' }}">
            <i class="fa fa-bullseye"></i>
            <span>Leistungen</span>
        </a>
    </li>
</ul>
{{ Form::open(['route' => 'logout']) }}
{{ Form::button('<i class="fa fa-sign-out fa-2x" aria-hidden="true"></i>', ['type' => 'submit', 'class' => 'logout bottom-left']) }}
{{ Form::close() }}