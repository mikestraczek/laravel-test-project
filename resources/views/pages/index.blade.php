@extends('layouts.default')

@section('title', 'Home')

@section('content')
    <div class="home-wrapper">
        <div class="page-content full-height">
            <div class="content">
                <div class="page-logo m-b-30">
                    <img src="{{ asset('images/logo.svg') }}" alt="Next Levels">
                </div>

                @include('partials.navigation')
            </div>
            @include('partials.footer-navigation')
            @include('partials.social-navigation')
        </div>
    </div>
@endsection