<nav>
    <ul class="navigation bottom-left footer">
        <li><a href="{{ route('impressum') }}">Impressum</a></li>
        <li><a href="{{ route('privacy') }}">Datenschutz</a></li>
        <li><a href="{{ route('contact') }}">Kontakt</a></li>
    </ul>
</nav>