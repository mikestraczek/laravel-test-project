<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <link rel="shortcut icon" type="image/x-icon" href="{{ asset('favicon.ico') }}">

        <title>@yield('title') - Next Levels</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">

    </head>
    <body>
        <div class="page-wrapper">
            <header class="header">
                @yield('header')
            </header>

            <div class="content-wrapper">
                @yield('content')
            </div>
        </div>

        <!-- Scripts -->
        <script async defer src="{{ asset('js/app.js') }}" type="text/javascript"></script>
    </body>
</html>
