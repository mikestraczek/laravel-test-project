<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

/**
 * Class UserController
 * @package App\Http\Controllers
 * @author Mike Straczek <mike.straczek@next-levels.de>, Next Levels GmbH
 */
class UserController extends BaseController
{

    use AuthenticatesUsers;

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.admin.auth.login');
    }
}
