@extends('layouts.admin.auth.default')

@section('title', 'Login')

@section('content')
    <div class="login-header">
        <h3>Login</h3>
    </div>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {{ Form::open(['route' => 'signin']) }}
    {{ Form::token() }}
        <div class="form-group">
            {{ Form::label('email', 'E-Mail') }}
            {{ Form::text('email', '', ['id' => 'email', 'class' => 'form-control']) }}
        </div>
        <div class="form-group">
            {{ Form::label('password', 'Passwort') }}
            {{ Form::password('password', ['id' => 'password', 'class' => 'form-control']) }}
        </div>
        {{ Form::button(
            'Einloggen<i class="fa fa-sign-in" aria-hidden="true"></i>',
            ['type' => 'submit', 'class' => 'btn btn-primary pull-right']
        ) }}
        <div class="clearfix"></div>
    {{ Form::close() }}
@endsection
