<?php

namespace App\Blog;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Post
 * @package App\Blog
 * @author  Mike Straczek <mike.straczek@next-levels.de>, Next Levels GmbH
 */
class Post extends Model
{

    use SoftDeletes;

    /**
     * @var string
     */
    protected $table = 'blog_posts';

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];
}
