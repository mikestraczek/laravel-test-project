@extends('layouts.admin.default')

@section('title', 'Blog')

@section('content')

    {{ Form::open(['route' => 'store-new-post']) }}
    <div class="form-group">
        {{ Form::label('title', 'Titel') }}
        {{ Form::text('title', '', ['class' => 'form-control', 'placeholder' => 'Titel']) }}
    </div>
    <div class="form-group">

        {{ Form::label('content', 'Inhalt') }}
        {{ Form::textarea('content', '', ['id' => 'blog_content', 'placeholder' => 'Inhalt']) }}
    </div>
    {{ Form::submit('Erstellen', ['class' => 'btn btn-primary']) }}
    {{ Form::close() }}
@endsection

@section('styles')
    <link href="{{ asset('dist/summernote/summernote-bs4.css') }}" rel="stylesheet">
@endsection

@section('scripts')
    <script src="{{ asset('dist/summernote/summernote-bs4.min.js') }}"></script>
    <script src="{{ asset('dist/summernote/lang/summernote-de-DE.js') }}"></script>

    <script>
        $(function () {
            $('#blog_content').summernote({
                placeholder: 'Inhalt',
                tabsize: 2,
                height: 300,
                lang: 'de-DE'
            });
        });
    </script>
@endsection