<?php

namespace App\Http\Controllers;

/**
 * Class UserController
 * @package App\Http\Controllers
 * @author Mike Straczek <mike.straczek@next-levels.de>, Next Levels GmbH
 */
class BaseController extends Controller
{

    /**
     * Return error messages
     *
     * @return array
     */
    public function getValidationMessages()
    {
        return [
            'required' => 'Das Feld :attribute muss ausgefüllt werden',
            'email.required' => 'Das Feld E-Mail muss ausgefüllt werden',
            'email.email' => 'Ihre Eingabe ist keine gültige E-Mail',
            'password.required' => 'Das Feld Passwort muss ausgefüllt werden'
        ];
    }
}
