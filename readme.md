# Laravel Test Projekt

## Setup

1. Clone repo: git clone -b develop --recursive https://bitbucket.org/mikestraczek/laravel-test-project.git
2. Execute follow command (Install PHP packages): `composer install`
3. Execute follow command (Install JavaScript packages): `npm install`
4. Copy .env.example file to root folder and rename the copy '.env' and modify database credetials (look below)
5. Copy .env file from 'data/laradock' folder to laradock folder
6. Execute follow command in 'laradock' folder (Start docker box): `docker-compose up -d --build nginx mysql`

## Credentials

### Lokal

#### Database
* Host: laradock_mysql_nl
* Database: default
* Username: root
* Password: root

## Compiling assets
* Run follow command to generate assets `npm run dev`
* Run follow command to trigger asset changes `npm run watch`