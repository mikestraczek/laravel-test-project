/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
window.$.fn.DataTable = require('datatables.net-bs');

/**
 * Check if any sub navigation item active and set main navigation item to active
 */
function checkIfSubNavigationActive() {
    $('.navigation li .sub-navigation').each(function (index, element) {

        if ($(element).find('li a.active').length !== 0) {
            $(element).slideToggle();
            $(element).prev().addClass('active sub-navigation-open');
        }
    });
}

$(function () {
    checkIfSubNavigationActive();

    /**
     * Open sub navigation on click
     */
    $('.navigation li a').click(function (event) {
        var subMenu = $(event.currentTarget).next('.sub-navigation');

        if (subMenu.length !== 0) {
            event.preventDefault();
            subMenu.slideToggle().prev().toggleClass('sub-navigation-open');
        }
    });
});