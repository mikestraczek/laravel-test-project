<?php

namespace App\Blog;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Category
 * @package App\Blog
 * @author  Mike Straczek <mike.straczek@next-levels.de>, Next Levels GmbH
 */
class Category extends Model
{

    use SoftDeletes;

    /**
     * @var string
     */
    protected $table = 'blog_categories';

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];
}
