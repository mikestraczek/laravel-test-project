@extends('layouts.admin.default')

@section('title', 'Blog')

@section('content')
<table class="table dataTable">
    <thead>
    <tr>
        <th>#</th>
        <th>Titel</th>
        <th>Vorschautext</th>
        <th>Inhalt</th>
        <th>Versteckt</th>
        <th>Erstellt am</th>
        <th>Geändert am</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    @if ($blogPosts->count())
        @foreach((array)$blogPosts as $blogPost)
            <tr>
                <td>{{ $blogPost->id }}</td>
                <td>{{ $blogPost->title }}</td>
                <td>{{ $blogPost->preview_text }}</td>
                <td>{{ $blogPost->content }}</td>
                <td>{{ $blogPost->hidden }}</td>
                <td>{{ $blogPost->created_at }}</td>
                <td>{{ $blogPost->updated_at }}</td>
                <td></td>
            </tr>
        @endforeach
    @else
        <tr>
            <td colspan="8">Keine Posts vorhanden</td>
        </tr>
    @endif
    </tbody>
</table>
@endsection
