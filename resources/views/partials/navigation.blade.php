<nav>
    <ul class="navigation page-navi">
        <li><a href="{{ route('home') }}">Home</a></li>
        <li><a href="{{ route('blog') }}">Blog</a></li>
        <li><a href="{{ route('performance') }}">Leistungen</a></li>
        <li><a href="{{ route('reference') }}">Referenzen</a></li>
        <li><a href="{{ route('admin-home') }}">Admin</a></li>
    </ul>
</nav>