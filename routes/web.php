<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('/')->group(function () {
    /**
     * Frontend Pages
     */
    Route::get('/', function () {
        return view('pages.index');
    })->name('home');

    Route::get('/impressum', function () {
        return view('pages.impressum');
    })->name('impressum');

    Route::get('/blog', function () {
        return view('pages.blog');
    })->name('blog');

    Route::get('/leistungen', function () {
        return view('pages.performance');
    })->name('performance');

    Route::get('/referenzen', function () {
        return view('pages.reference');
    })->name('reference');

    Route::get('/kontakt', function () {
        return view('pages.contact');
    })->name('contact');

    Route::get('/datenschutz', function () {
        return view('pages.privacy');
    })->name('privacy');

    Route::get('/login', function () {
        return redirect('/admin/login');
    });

    /**
     * Admin Pages
     */
    Route::prefix('admin')->group(function () {
        Route::namespace('Admin')->group(function () {
            Route::middleware('auth')->group(function () {
                Route::get('/', 'AdminController@index')->name('admin-home');

                Route::prefix('blog')->group(function () {
                    Route::prefix('posts')->group(function () {
                        Route::get('/', 'Blog\PostsController@index')->name('posts');
                        Route::get('posts/new', 'Blog\PostsController@create')->name('new-post');
                        Route::post('posts/new', 'Blog\PostsController@store')->name('store-new-post');
                    });
                });

                Route::get('leistungen', function () {
                    return view('pages.admin.performance');
                })->name('admin-performance');

                Route::get('referenzen', function () {
                    return view('pages.admin.reference');
                })->name('admin-reference');
            });

            Route::get('login', 'UserController@index')->name('login');
            Route::post('login', 'UserController@login')->name('signin');
            Route::post('logout', 'UserController@logout')->name('logout');
        });
    });
});
